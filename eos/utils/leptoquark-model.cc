/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Ismo Toijala
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <eos/utils/leptoquark-model.hh>
#include <eos/utils/model.hh>
#include <eos/utils/power_of.hh>
#include <eos/utils/standard-model.hh>
#include <eos/utils/stringify.hh>

#include <complex>

namespace eos
{
    namespace lqimplementation
    {
        complex<double> polar(const Parameter & abs, const Parameter & arg) { return std::polar(abs(), arg()); }
        complex<double> cartesian(const Parameter & re, const Parameter & im) { return complex<double>(re(), im()); }
    }

    LeptoquarkComponent<components::DeltaB1>::LeptoquarkComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _sm__deltab1(Model::make("SM", p, o)),
        _wilson__deltab1(Model::make("WilsonScan", p, o)),
        _lq_lepton__deltab1(o.get("lq-lepton", "mu")),
        _lq_su2__deltab1(o.get("lq-su2", "2")),
        _G_F__deltab1(p["G_Fermi"], u),
        _alpha_e__deltab1(p["QED::alpha_e(m_b)"], u),
        _mu__deltab1(p["mu"], u),
        _M_LQ__deltab1(p["M_LQ"], u),
        _abs_ll_LQ__deltab1(p["Abs{ll_LQ}"]),
        _arg_ll_LQ__deltab1(p["Arg{ll_LQ}"]),
        _re_ll_LQ__deltab1(p["Re{ll_LQ}"]),
        _im_ll_LQ__deltab1(p["Im{ll_LQ}"])
    {
        if ("e" != _lq_lepton__deltab1 && "mu" != _lq_lepton__deltab1 && "tau" != _lq_lepton__deltab1)
        {
            throw InternalError("LeptoquarkModel: '" + _lq_lepton__deltab1 + "' is not a valid lepton");
        }

        if ("2" != _lq_su2__deltab1 && "3" != _lq_su2__deltab1)
        {
            throw InternalError("LeptoquarkModel: '" + _lq_su2__deltab1 + "' is not in {2, 3}");
        }

        u.uses(*_sm__deltab1);
        u.uses(*_wilson__deltab1);

        if ("polar" == o.get("scan-mode", "polar"))
        {
            _ll_LQ__deltab1 = std::bind(&lqimplementation::polar, _abs_ll_LQ__deltab1, _arg_ll_LQ__deltab1);
            u.uses(_abs_ll_LQ__deltab1.id()); u.uses(_arg_ll_LQ__deltab1.id());
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _ll_LQ__deltab1 = std::bind(&lqimplementation::cartesian, _re_ll_LQ__deltab1, _im_ll_LQ__deltab1);
            u.uses(_re_ll_LQ__deltab1.id()); u.uses(_im_ll_LQ__deltab1.id());
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for LeptoquarkModel");
        }
    }

    WilsonCoefficients<BToS>
    LeptoquarkComponent<components::DeltaB1>::wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const
    {
        if ("e" != lepton && "mu" != lepton && "tau" != lepton)
        {
            throw InternalError("LeptoquarkModel: '" + lepton + "' is not a valid lepton");
        }

        WilsonCoefficients<BToS> wc = _wilson__deltab1->wilson_coefficients_b_to_s(lepton, false);

        if (lepton == _lq_lepton__deltab1)
        {
            const complex<double> C = - _ll_LQ__deltab1() / (4 * power_of<2>(_M_LQ__deltab1()))
                                      / (4 / sqrt(2) * _G_F__deltab1() * _sm__deltab1->ckm_tb() * std::conj(_sm__deltab1->ckm_ts()))
                                      / (_alpha_e__deltab1() / (4 * M_PI))
                                      * (_sm__deltab1->alpha_s(_mu__deltab1()) / (4 * M_PI));

            if ("2" == _lq_su2__deltab1)
            {
                wc._primed_coefficients[13] += + C;
                wc._primed_coefficients[14] += - C;
            }
            else
            {
                wc._sm_like_coefficients[13] += +2. * C;
                wc._sm_like_coefficients[14] += -2. * C;
            }
        }

        if (cp_conjugate)
        {
            for (auto & c : wc._sm_like_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : wc._primed_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : wc._scalar_tensor_coefficients)
            {
                c = conj(c);
            }
        }

        return wc;
    }

    LeptoquarkComponent<components::DeltaB2>::LeptoquarkComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _sm__deltab2(Model::make("SM", p, o)),
        _wilson__deltab2(Model::make("WilsonScan", p, o)),
        _lq_su2__deltab2(o.get("lq-su2", "2")),
        _G_F__deltab2(p["G_Fermi"], u),
        _m_W__deltab2(p["mass::W"], u),
        _M_LQ__deltab2(p["M_LQ"], u),
        _abs_ll_LQ__deltab2(p["Abs{ll_LQ}"]),
        _arg_ll_LQ__deltab2(p["Arg{ll_LQ}"]),
        _re_ll_LQ__deltab2(p["Re{ll_LQ}"]),
        _im_ll_LQ__deltab2(p["Im{ll_LQ}"])
    {
        if ("2" != _lq_su2__deltab2 && "3" != _lq_su2__deltab2)
        {
            throw InternalError("LeptoquarkModel: '" + _lq_su2__deltab2 + "' is not in {2, 3}");
        }

        u.uses(*_sm__deltab2);
        u.uses(*_wilson__deltab2);

        if ("polar" == o.get("scan-mode", "polar"))
        {
            _ll_LQ__deltab2 = std::bind(&lqimplementation::polar, _abs_ll_LQ__deltab2, _arg_ll_LQ__deltab2);
            u.uses(_abs_ll_LQ__deltab2.id()); u.uses(_arg_ll_LQ__deltab2.id());
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _ll_LQ__deltab2 = std::bind(&lqimplementation::cartesian, _re_ll_LQ__deltab2, _im_ll_LQ__deltab2);
            u.uses(_re_ll_LQ__deltab2.id()); u.uses(_im_ll_LQ__deltab2.id());
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for LeptoquarkModel");
        }
    }

    WilsonCoefficients<DeltaB2>
    LeptoquarkComponent<components::DeltaB2>::wilson_coefficients_deltab2(const std::string & quark, const bool & cp_conjugate) const
    {
        WilsonCoefficients<DeltaB2> wc = _wilson__deltab2->wilson_coefficients_deltab2(quark, false);

        if ("s" == quark)
        {
            const complex<double> C = power_of<2>(_ll_LQ__deltab2()) / (64 * power_of<2>(M_PI) * power_of<2>(_M_LQ__deltab2()))
                                      / (power_of<2>(_G_F__deltab2() * _m_W__deltab2()) / (16 * power_of<2>(M_PI)))
                                      / (power_of<2>(_sm__deltab2->ckm_tb() * std::conj(_sm__deltab2->ckm_ts())));

            if ("2" == _lq_su2__deltab2)
            {
                wc._c += C;
            }
            else
            {
                wc._c += 5. / 2. * C;
            }
        }

        if (cp_conjugate)
        {
            wc._c = conj(wc._c);
        }

        return wc;
    }

    LeptoquarkComponent<components::DeltaB2>::LeptoquarkComponent(const Parameters &, const Options &, ParameterUser &);

    WilsonCoefficients<DeltaB2>
    LeptoquarkComponent<components::DeltaB2>::wilson_coefficients_deltab2(const std::string & quark, const bool & cp_conjugate) const;

    LeptoquarkModel::LeptoquarkModel(const Parameters & p, const Options & o) :
        SMComponent<components::CKM>(p, *this),
        SMComponent<components::QCD>(p, *this),
        LeptoquarkComponent<components::DeltaB1>(p, o, *this),
        LeptoquarkComponent<components::DeltaB2>(p, o, *this)
    {
    }

    LeptoquarkModel::~LeptoquarkModel()
    {
    }

    std::shared_ptr<Model>
    LeptoquarkModel::make(const Parameters & parameters, const Options & options)
    {
        return std::shared_ptr<Model>(new LeptoquarkModel(parameters, options));
    }
}
