/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Dennis Loose
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * deltails.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#ifndef EOS_GUARD_SRC_UTILS_RPVMSSM_MODEL_HH
#define EOS_GUARD_SRC_UTILS_RPVMSSM_MODEL_HH 1

#include <eos/utils/model.hh>
#include <eos/utils/standard-model.hh>

namespace eos
{
    template <typename Tag> class RPVMSSMComponent;

    template <> class RPVMSSMComponent<components::DeltaB1> :
        public virtual ModelComponent<components::DeltaB1>
    {
        private:
            std::shared_ptr<Model> _sm__deltab1;

            std::shared_ptr<Model> _wilsonscan__deltab1;

            UsedParameter _G_F;

            UsedParameter _alpha_e;

            UsedParameter _m_snu;

            Parameter _abs_LAMBDA_e, _arg_LAMBDA_e;

            Parameter _abs_LAMBDA_prime_e, _arg_LAMBDA_prime_e;

            Parameter _re_LAMBDA_e, _im_LAMBDA_e;

            Parameter _re_LAMBDA_prime_e, _im_LAMBDA_prime_e;

            Parameter _abs_LAMBDA_mu, _arg_LAMBDA_mu;

            Parameter _abs_LAMBDA_prime_mu, _arg_LAMBDA_prime_mu;

            Parameter _re_LAMBDA_mu, _im_LAMBDA_mu;

            Parameter _re_LAMBDA_prime_mu, _im_LAMBDA_prime_mu;

            std::function<complex<double> ()> _LAMBDA_e;

            std::function<complex<double> ()> _LAMBDA_prime_e;

            std::function<complex<double> ()> _LAMBDA_mu;

            std::function<complex<double> ()> _LAMBDA_prime_mu;

        public:
            RPVMSSMComponent(const Parameters &, const Options &, ParameterUser &);

            virtual WilsonCoefficients<BToS> wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const;
    };

 class RPVMSSMModel :
        public Model,
        public SMComponent<components::CKM>,
        public SMComponent<components::QCD>,
        public RPVMSSMComponent<components::DeltaB1>,
        public SMComponent<components::DeltaB2>
    {
        public:
            RPVMSSMModel(const Parameters &, const Options &);
            virtual ~RPVMSSMModel();

            static std::shared_ptr<Model> make(const Parameters &, const Options &);
    };
}

#endif
