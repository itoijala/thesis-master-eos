/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Niklas Bonacker
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <eos/utils/zprime-model.hh>
#include <eos/utils/model.hh>
#include <eos/utils/power_of.hh>
#include <eos/utils/standard-model.hh>
#include <eos/utils/stringify.hh>

#include <complex>

namespace eos
{
    namespace zpimplementation
    {
        complex<double> polar(const Parameter & abs, const Parameter & arg) { return std::polar(abs(), arg()); }
        complex<double> cartesian(const Parameter & re, const Parameter & im) { return complex<double>(re(), im()); }
    }

    ZPrimeComponent<components::DeltaB1>::ZPrimeComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _sm__deltab1(Model::make("SM", p, o)),
        _wilson__deltab1(Model::make("WilsonScan", p, o)),
        _G_F__deltab1(p["G_Fermi"], u),
        _alpha_e__deltab1(p["QED::alpha_e(m_b)"], u),
        _mu__deltab1(p["mu"], u),
        _m_Q__deltab1(p["m_Q"], u),
        _m_D__deltab1(p["m_D"], u),
        _abs_YY_Q__deltab1(p["Abs{YY_Q}"]),
        _arg_YY_Q__deltab1(p["Arg{YY_Q}"]),
        _re_YY_Q__deltab1(p["Re{YY_Q}"]),
        _im_YY_Q__deltab1(p["Im{YY_Q}"]),
        _abs_YY_D__deltab1(p["Abs{YY_D}"]),
        _arg_YY_D__deltab1(p["Arg{YY_D}"]),
        _re_YY_D__deltab1(p["Re{YY_D}"]),
        _im_YY_D__deltab1(p["Im{YY_D}"])

    {
        u.uses(*_sm__deltab1);
        u.uses(*_wilson__deltab1);

        if ("polar" == o.get("scan-mode", "polar"))
        {
            _YY_Q__deltab1 = std::bind(&zpimplementation::polar, _abs_YY_Q__deltab1, _arg_YY_Q__deltab1);
            u.uses(_abs_YY_Q__deltab1.id()); u.uses(_arg_YY_Q__deltab1.id());

            _YY_D__deltab1 = std::bind(&zpimplementation::polar, _abs_YY_D__deltab1, _arg_YY_D__deltab1);
            u.uses(_abs_YY_D__deltab1.id()); u.uses(_arg_YY_D__deltab1.id());
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _YY_Q__deltab1 = std::bind(&zpimplementation::cartesian, _re_YY_Q__deltab1, _im_YY_Q__deltab1);
            u.uses(_re_YY_Q__deltab1.id()); u.uses(_im_YY_Q__deltab1.id());

            _YY_D__deltab1 = std::bind(&zpimplementation::cartesian, _re_YY_D__deltab1, _im_YY_D__deltab1);
            u.uses(_re_YY_D__deltab1.id()); u.uses(_im_YY_D__deltab1.id());
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for LeptoquarkModel");
        }
    }

    WilsonCoefficients<BToS>
    ZPrimeComponent<components::DeltaB1>::wilson_coefficients_b_to_s(const std::string & lepton, const bool & cp_conjugate) const
    {
        if (lepton != "e" && lepton != "mu" && lepton != "tau")
        {
            throw InternalError("ZPrimeModel: '" + lepton + "' os not a valid lepton flavour");
        }

        WilsonCoefficients<BToS> wc = _wilson__deltab1->wilson_coefficients_b_to_s(lepton, false);

        if (lepton == "mu")
        {
            const complex<double> C_Q = - _YY_Q__deltab1() / (2 * power_of<2>(_m_Q__deltab1()))
                                        / (4 / sqrt(2) * _G_F__deltab1() * _sm__deltab1->ckm_tb() * conj(_sm__deltab1->ckm_ts()))
                                        / (_alpha_e__deltab1() / (4 * M_PI))
                                        * (_sm__deltab1->alpha_s(_mu__deltab1()) / (4 * M_PI));
            const complex<double> C_D = + _YY_D__deltab1() / (2 * power_of<2>(_m_D__deltab1()))
                                        / (4 / sqrt(2) * _G_F__deltab1() * _sm__deltab1->ckm_tb() * conj(_sm__deltab1->ckm_ts()))
                                        / (_alpha_e__deltab1() / (4 * M_PI))
                                        * (_sm__deltab1->alpha_s(_mu__deltab1()) / (4 * M_PI));

            wc._sm_like_coefficients[13] +=  C_Q;
            wc._primed_coefficients[13] +=  C_D;
        }

        if (cp_conjugate)
        {
            for (auto & c : wc._sm_like_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : wc._primed_coefficients)
            {
                c = conj(c);
            }

            for (auto & c : wc._scalar_tensor_coefficients)
            {
                c = conj(c);
            }
        }

        return wc;
    }

    ZPrimeComponent<components::DeltaB2>::ZPrimeComponent(const Parameters & p, const Options & o, ParameterUser & u) :
        _sm__deltab2(Model::make("SM", p, o)),
        _wilson__deltab2(Model::make("WilsonScan", p, o)),
        _G_F__deltab2(p["G_Fermi"], u),
        _m_W__deltab2(p["mass::W"], u),
        _v_Phi__deltab2(p["v_Phi"], u),
        _m_Q__deltab2(p["m_Q"], u),
        _m_D__deltab2(p["m_D"], u),
        _abs_YY_Q__deltab2(p["Abs{YY_Q}"]),
        _arg_YY_Q__deltab2(p["Arg{YY_Q}"]),
        _re_YY_Q__deltab2(p["Re{YY_Q}"]),
        _im_YY_Q__deltab2(p["Im{YY_Q}"]),
        _abs_YY_D__deltab2(p["Abs{YY_D}"]),
        _arg_YY_D__deltab2(p["Arg{YY_D}"]),
        _re_YY_D__deltab2(p["Re{YY_D}"]),
        _im_YY_D__deltab2(p["Im{YY_D}"])
    {
        u.uses(*_sm__deltab2);
        u.uses(*_wilson__deltab2); 

        if ("polar" == o.get("scan-mode", "polar"))
        {
            _YY_Q__deltab2 = std::bind(&zpimplementation::polar, _abs_YY_Q__deltab2, _arg_YY_Q__deltab2);
            u.uses(_abs_YY_Q__deltab2.id()); u.uses(_arg_YY_Q__deltab2.id());

            _YY_D__deltab2 = std::bind(&zpimplementation::polar, _abs_YY_D__deltab2, _arg_YY_D__deltab2);
            u.uses(_abs_YY_D__deltab2.id()); u.uses(_arg_YY_D__deltab2.id());
        }
        else if ("cartesian" == o.get("scan-mode", "polar"))
        {
            _YY_Q__deltab2 = std::bind(&zpimplementation::cartesian, _re_YY_Q__deltab2, _im_YY_Q__deltab2);
            u.uses(_re_YY_Q__deltab2.id()); u.uses(_im_YY_Q__deltab2.id());

            _YY_D__deltab2 = std::bind(&zpimplementation::cartesian, _re_YY_D__deltab2, _im_YY_D__deltab2);
            u.uses(_re_YY_D__deltab2.id()); u.uses(_im_YY_D__deltab2.id());
        }
        else
        {
            throw InternalError("scan-mode = '" + stringify(o.get("scan-mode", "polar")) + "' is not a valid scan mode for ZPrimeModel");
        }
    }

    WilsonCoefficients<DeltaB2>
    ZPrimeComponent<components::DeltaB2>::wilson_coefficients_deltab2(const std::string & quark, const bool & cp_conjugate) const
    {
        WilsonCoefficients<DeltaB2> wc = _wilson__deltab2->wilson_coefficients_deltab2(quark, false);

        if ("s" == quark)
        {
            const complex<double> C_LL = power_of<2>(_YY_Q__deltab2()) * (power_of<2>(_v_Phi__deltab2()) / power_of<4>(_m_Q__deltab2()) + 1 / power_of<2>(4 * M_PI * _m_Q__deltab2()));
            const complex<double> C_RR = power_of<2>(_YY_D__deltab2()) * (power_of<2>(_v_Phi__deltab2()) / power_of<4>(_m_D__deltab2()) + 1 / power_of<2>(4 * M_PI * _m_D__deltab2()));

            const double temp = log(power_of<2>(_m_Q__deltab2() / _m_D__deltab2())) / (power_of<2>(4 * M_PI) * (power_of<2>(_m_Q__deltab2()) - power_of<2>(_m_D__deltab2())));
            const complex<double> C_LR = _YY_Q__deltab2() * _YY_D__deltab2() * (power_of<2>(_v_Phi__deltab2() / (_m_Q__deltab2() * _m_D__deltab2())) + (std::isnan(temp) ? 0.0 : temp));

            wc._c += (C_LL + C_RR + 9.7 * C_LR)
                     / (power_of<2>(_G_F__deltab2() * _m_W__deltab2()) / (16 * power_of<2>(M_PI)) * power_of<2>(_sm__deltab2->ckm_tb() * std::conj(_sm__deltab2->ckm_ts())));
        }

        if (cp_conjugate)
        {
            wc._c = conj(wc._c);
        }

        return wc;
    }

    ZPrimeModel::ZPrimeModel(const Parameters & p, const Options & o) :
        SMComponent<components::CKM>(p, *this),
        SMComponent<components::QCD>(p, *this),
        ZPrimeComponent<components::DeltaB1>(p, o, *this),
        ZPrimeComponent<components::DeltaB2>(p, o, *this)
    {
    }

    ZPrimeModel::~ZPrimeModel()
    {
    }

    std::shared_ptr<Model>
    ZPrimeModel::make(const Parameters & parameters, const Options & options)
    {
        return std::shared_ptr<Model>(new ZPrimeModel(parameters, options));
    }
}
