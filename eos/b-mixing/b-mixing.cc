/* vim: set sw=4 sts=4 et foldmethod=syntax : */

/*
 * Copyright (c) 2015 Ismo Toijala
 *
 * This file is part of the EOS project. EOS is free software;
 * you can redistribute it and/or modify it under the terms of the GNU General
 * Public License version 2, as published by the Free Software Foundation.
 *
 * EOS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include <eos/b-mixing/b-mixing.hh>
#include <eos/utils/model.hh>
#include <eos/utils/power_of.hh>
#include <eos/utils/private_implementation_pattern-impl.hh>

namespace eos
{
    template <>
    struct Implementation<BMixing>
    {
        std::shared_ptr<Model> model;

        UsedParameter f_B;

        UsedParameter m_B;

        UsedParameter g_fermi;

        UsedParameter hbar;

        UsedParameter m_W;

        UsedParameter Bhat_B;

        std::function<complex<double> (const Model *)> lambda;

        std::string quark;

        Implementation(const Parameters & p, const Options & o, ParameterUser & u) :
            model(Model::make(o.get("model", "SM"), p, o)),
            f_B(p["decay-constant::B_" + o.get("q", "d")], u),
            m_B(p["mass::B_" + o.get("q", "d")], u),
            g_fermi(p["G_Fermi"], u),
            hbar(p["hbar"], u),
            m_W(p["mass::W"], u),
            Bhat_B(p["bag-parameter::B_" + o.get("q", "d")], u),
            quark(o.get("q", "d"))
        {
            if (o.get("q", "d") == "d")
            {
                lambda = &lambda_t_d;
            }
            else if (o.get("q", "d") == "s")
            {
                lambda = &lambda_t_s;
            }
            else
            {
                // only neutral B mesons can mix
                throw InternalError("BMixing: q = '" + o["q"] + "' is not a valid option for mixing");
            }

            u.uses(*model);
        }

        // CKM factors
        static complex<double> lambda_t_d(const Model * model) { return model->ckm_tb() * conj(model->ckm_td()); }
        static complex<double> lambda_t_s(const Model * model) { return model->ckm_tb() * conj(model->ckm_ts()); }

        // ps^-1
        double mass_difference() const
        {
            WilsonCoefficients<DeltaB2> wc = model->wilson_coefficients_deltab2(quark);

            return power_of<2>(g_fermi() * m_W()) * m_B() / (6 * power_of<2>(M_PI)) * power_of<2>(abs(lambda(model.get()))) * abs(wc.c()) * power_of<2>(f_B()) * Bhat_B() / hbar() * 1e-12;
        }

        double phase() const
        {
            WilsonCoefficients<DeltaB2> wc = model->wilson_coefficients_deltab2(quark);

            return arg(wc.c());
        }
    };

    BMixing::BMixing(const Parameters & parameters, const Options & options) :
        PrivateImplementationPattern<BMixing>(new Implementation<BMixing>(parameters, options, *this))
    {
    }

    BMixing::~BMixing()
    {
    }

    double BMixing::mass_difference() const
    {
        return _imp->mass_difference();
    }

    double BMixing::phase() const
    {
        return _imp->phase();
    }
}
